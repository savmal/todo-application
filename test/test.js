const expect = require('chai').expect;
const request = require('request');
const {ObjectID} = require('mongodb');

const {app} = require('./../server');
//const {Todo} = require('./../client/src/App');
const {todos, addDummyTodoItems} = require('./seed/seed')

beforeEach(addDummyTodoItems);

describe('POST /add', () => {
  it('should create a new todo', (done) => {
    var text = 'Testing todo route';

    request(app)
      .post('/add')
      .send({text})
      .expect(200)
      .expect((res) => {
        expect(res.body.text).toBe(text);
      })
      .end((err, res) => {
        if(err) return done(err);

        Todo.find({text}).then((todos) => {
          expect(todos.length).toBe(1);
          expect(todos[0].text).toBe(text);
          done();
        }).catch((err) => {
          done(err);
        });
      });
  });

  it('should not create todo with invaild body data', (done) => {
    request(app)
      .post('/add')
      .send({})
      .expect(400)
      .end((err, res) => {
        if(err) return done(err);

        Todo.find().then((todos) => {
          expect(todos.length).toBe(2);
          done();
        }).catch((err) => {
          done(err);
        })
      })
  });
});

describe('GET /', () => {
  it('should get all todos', (done) => {
    request(app)
      .get('/')
      .expect(200)
      .expect((res) => {
        expect(res.body.todos.length).toBe(1)
      })
      .end(done);
  })
});

describe('GET /:id', () => {
  it('should return todo by id', (done) => {
    request(app)
      .get(`/${todos[0]._id.toHexString()}`)
      .expect(200)
      .expect((res) => {
        expect(res.body.todo.text).toBe(todos[0].text);
      })
      .end(done);
  });

  it('Should return 404 if todo is not found', (done) => {
    var hexID = new ObjectID().toHexString();

    request(app)
      .get(`/${hexID}`)
      .expect(404)
      .end(done);
  })
});

describe('POST /update/:id', () => {
    it('Should update a todo', (done) => {
      var hexId = todos[1]._id.toHexString();
  
      request(app)
        .delete(`/update/${hexId}`)
        .expect(200)
        .expect((res) => {
          expect(res.body.todo._id).toBe(hexId);
        })
        .end((err, res) => {
          if(err) return done(err);
  
          Todo.findById(hexId).then((todo) => {
            expect(todo).toBeNull();
            done();
          }).catch((err) => {
            done(err);
          })
        })
    });
});
  
describe('DELETE /delete/:id', () => {
  it('Should remove a todo', (done) => {
    var hexId = todos[1]._id.toHexString();

    request(app)
      .delete(`/delete/${hexId}`)
      .expect(200)
      .expect((res) => {
        expect(res.body.todo._id).toBe(hexId);
      })
      .end((err, res) => {
        if(err) return done(err);

        Todo.findById(hexId).then((todo) => {
          expect(todo).toBeNull();
          done();
        }).catch((err) => {
          done(err);
        })
      })
  });
});