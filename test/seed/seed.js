const {ObjectID} = require('mongodb');

const {Todo} = require('./../../todo.model');

const todos = [{
  _id: new ObjectID(),
  title: "First test todo",
  completed: false
}, {
  _id: new ObjectID(),
  title: "Second test todo",
  completed: true
}];

var addDummyTodoItems = (done) => {
  Todo.deleteMany({}).then(() => {
    return Todo.insertMany(todos);
  }).then(() => done());
};

module.exports = {
  todos,
  addDummyTodoItems,
}